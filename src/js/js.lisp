(in-package :cl-naive-dom.js)

(defparameter *indent-size* 2)

(defparameter *indent* 0)

(defmethod emit-element ((document-format (eql :js)) tag element path &key parent-el)
  (declare (ignore tag))
  (let ((el (gensym "el")))
    (format nil
            "~A~%~{~A~^~%~}~%~{~A~^~%~}~%~A"
            (parenscript:ps*
             `(progn
                (parenscript:var ,el
                                 (parenscript:chain document (create-element ,(string (tag element)))))))

            (let ((attrs))
              (dolist (att (attributes element))
                (when (value att)
                  (push (parenscript:ps*
                         `(parenscript:chain ,el (set-attribute ,(key att) ,(value att))))
                        attrs)))
              attrs)

            (let ((emits))
              (dolist (child (children element))
                (push (emit-dom document-format child path :parent-el el)
                      emits))
              (reverse emits))

            (parenscript:ps*
             (if parent-el
                 `(parenscript:chain ,parent-el
                                     (append-child ,el))
                 `(parenscript:chain document body
                                     (append-child ,el)))))))

(defmethod emit-dom ((document-format (eql :js)) element path &key parent-el)
  (etypecase element
    (string
     (str:replace-all "innerHtml" "innerHTML"
                      (parenscript:ps* `(setf (parenscript:chain ,parent-el inner-html)
                                              (+ (parenscript:chain ,parent-el inner-html)
                                                 ;;TODO: Consider
                                                 ;;making it spans and
                                                 ;;not just a space
                                                 ;;seperation.
                                                 ,(format nil "~a " element))))))
    (number
     element)
    (keyword
     element)
    (element
     (push element path)
     (emit-element document-format (tag element) element path :parent-el parent-el))
    (list
     (format nil "~{~A~^~%~}"
             (mapcar (lambda (elm)
                       (emit-dom document-format elm path :parent-el parent-el))
                     element)))))

(defmacro with-js (&body body)
  "Macro used to emit js code that can be used to create the document in the browser dom."
  `(emit-dom :js (cl-naive-dom:expand (with-dom ,@body)) nil))

