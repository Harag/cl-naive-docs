(in-package :cl-naive-dom)

(defparameter *tags*
  (make-hash-table
   :test 'equalp
   #+(or sbcl ecl) :synchronized
   #+(or sbcl ecl) t
   #+ccl :shared #+ccl t)
  "Tags defined with deftag, used during parsing and transforms.

The *TAGS* hash-table maps the converted tag symbols to the \"original\" tag-name.
")

(defvar *transforms* (make-hash-table)
  "Maps transform names to transform instances.")

(defun intern-tag (tag-string)
  "Tag descriptors/name are stored internally as symbols to take
advantage of packages. So if the tag descriptor is not a symbol the
descriptor is converted to a symbol.

FULLSTOP (.) is used to designate a package only the first #\\. is considered.

Examples:

:mypackage.mytag will be converted to 'mypackage:mytag

:mypackage.myfancy-tag.thingy.mebob will be converted to 'mypackage:myfancy-tag.thingy.mebob

:mytag will be converted to '[*package*].mytag depending on what *package* is at the time.

If a package is explicitly given and cannot be found an error is raised.
"
  (let ((split (split-sequence:split-sequence #\. tag-string)))
    (if (= 1 (length split))
        (intern (string-upcase (first split)) *package*)
        (intern (format nil "~:@(~{~:a~^.~}~)" (cdr split))
                (or (find-package (string-upcase (first split)))
                    (error "There's no package named ~S for tag ~S"
                           (string-upcase (first split))
                           tag-string))))))

(defun tag-convert (tag)
  "Tags names can be keywords or symbols. This function converts the TAG to a symbol."
  (if (keywordp tag)
      (intern-tag (symbol-name tag))
      tag))

(defun convert-tags (tags)
  "Converts a list of tags to their correct symbol equivilant."
  (mapcar (function tag-convert) tags))

(defun default-transform (tag)
  "The default transformation of tag. This transformation is used to
EXPAND elements with. The default transformation is supplied in DEFTAG."
  (find-transform-named (tag-convert tag)))

(defun tag-prep (tag)
  "Prepares tags for use in tag-equal-p."
  (cond
    ((keywordp tag)
     (tag-convert tag))
    ((consp tag)
     (convert-tags tag))
    (t
     (typecase tag
       (element
        (tag tag))
       (otherwise
        tag)))))

(defun tag-equal-p (tag tagx)
  "Compares TAG and TAGX to see if the tag-names match. Tag names are
stored interally as symbols (with their package), that way the system
can differentiate between tags form different packages.

Both parameters can nearly be anything that could represent a tag. A
list of possible value type is KEYWORD, SYMBOL, ELEMENT or a list of
one of those.

When two lists of tags are supplied an intersection will return true.
When a tag and a list of tags are supplied the test is member."
  (let ((tag-prepped (tag-prep tag))
        (tagx-prepped (tag-prep tagx)))
    (if (consp tag-prepped)
        (if (consp tagx-prepped)
            (intersection tag-prepped tagx-prepped)
            (member tagx-prepped tag-prepped))
        (if (consp tagx-prepped)
            (member tag-prepped tagx-prepped)
            (eql tag-prepped tagx-prepped)))))

(defun ensure-tag (tag)
  (let ((converted-tag  (tag-convert tag)))
    ;; Note: tag could be NIL, so we cannot just use gethash to check a tag.
    ;; See TAG-P
    (setf (gethash converted-tag *tags*) tag)))

(defvar *disabled-tags* nil
  "List of tags that must not be considered as tags temporarily.")

(defun tag-p (tag)
  "Has the tag been defined with DEFTAG."
  (unless (member tag *disabled-tags*)
    ;; Note: tag could be NIL, so we cannot just use gethash to check a tag.
    ;; See what is set in *tags* in ENSURE-TAG
    (nth-value 1 (gethash (tag-convert tag) *tags*))))

(defmacro with-disabled-tags (list-of-tags &body body)
  "Disable considering the given symbols as tags.
When parsing code, if a local function or macro binding is made with a tag name, this is not a tag for the scope!"
  `(let ((*disabled-tags* (append ,list-of-tags *disabled-tags*)))
     ,@body))
