(in-package :cl-naive-dom)

(declaim (ftype function tag-convert))

(defclass selector ()
  ())

(defgeneric select (selector element &key path)
  (:documentation "Applies the SELECTOR to the ELEMENT.
Return whether the ELEMENT is selected.
PATH is the list of elements from the parent of ELEMENT up to the root.")
  (:method ((selector t) element &key path)
    (declare (ignore element path))
    nil)
  (:method ((selector symbol) element &key path)
    (declare (ignore path))
    ;; (tag-equal-p selector element)
    (eql (tag-convert selector) (tag element)))
  (:method ((selector cons) element &key path)
    (declare (ignore path))
    ;; (tag-equal-p selector element)
    (member (tag element) (mapcar (function tag-convert) selector)))
  (:method ((selector null) element &key path)
    (declare (ignore element path))
    t))

(defclass function-selector (selector)
  ((predicate :initarg :predicate
              :type function
              :reader function-selector-predicate)))

(defmethod print-object ((selector function-selector) stream)
  (print-unreadable-object (selector stream :identity t :type t)
    (format stream "~{~S~^ ~}" (list :predicate (function-selector-predicate selector))))
  selector)

(defmethod select ((selector function-selector) element &key path)
  (funcall (function-selector-predicate selector) element path))

(defclass tag-selector (selector)
  ((tags :initarg :tags
         :type list
         :reader tag-selector-tags)))

(defmethod print-object ((selector tag-selector) stream)
  (print-unreadable-object (selector stream :identity t :type t)
    (format stream "~{~S~^ ~}" (list :tags (tag-selector-tags selector))))
  selector)

(defmethod select ((selector tag-selector) element &key path)
  (declare (ignore path))
  ;;Converting tags just in case
  (let ((tags (convert-tags (tag-selector-tags selector))))
    (if tags
        (if (member (tag element) (mapcar (function tag-convert) tags))
            t
            nil)
        t)))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun selector* (transform-selector)
    (cond
      ((null transform-selector)
       'nil)
      ((or (and (symbolp transform-selector)
                (not (keywordp transform-selector)))
           (lambda-expression-p transform-selector))
       `(make-instance 'function-selector
                       :predicate (function ,transform-selector)))
      (t
       (let ((converted-selectors (loop :for selector in (uiop:ensure-list transform-selector)
                                        :collect (tag-convert selector))))
         `(make-instance 'tag-selector
                         :tags ',converted-selectors))))))

(defmacro selector (transform-selector)
  (selector* transform-selector))

(defgeneric post-walk (tree fun path)
  (:documentation
   "Call FUN on each node of the TREE, with the new children, in a post-walk.

RETURN:  The result of FUN.

FUN:  (function (element list) (or element list))
      The function takes the current ELEMENT, and a list of new children
      computed by the post-walk of each children of the ELEMENT.
      It returns either a list (possibly empty) of replacement elements,
      or a single element instance.

PATH: The path from the current node up to the root, as a list of nodes.
"))

(defmethod post-walk ((node t) fun path)
  (declare (ignore fun path))
  node)

(defmethod post-walk ((node element) fun path)
  (let* ((new-path (cons node path))
         (new-children
           (mapcan (lambda (child)
                     (uiop:ensure-list (post-walk child fun new-path)))
                   (children node))))
    (funcall fun node new-children path)))

;; A transformation is a function that takes an element with a
;; specific tag, and returns a new element (or possibly the same). We
;; keep those transformations in lists aka chains, that can be applied
;; one after the other, to transform a DOM.  The transforms are
;; applied during a post-walk of the DOM tree (one post walk per
;; transform, since the next transform could process the newly
;; generated element).

(defclass transform ()
  ((name :initarg :name
         :type symbol
         :reader transform-name)
   (selector :initarg :selector
             :initform nil
             :type (or null selector)
             :reader transform-selector)
   (function :initarg :function
             :type function
             :reader transform-function)))

(defvar *transforms* (make-hash-table)
  "Maps transform names to transform instances.")

(defun find-transform-named (name)
  "Return the transform instance that has this NAME, or NIL if none."
  (gethash name *transforms*))

(defmacro transform (name)
  "Return the tranform instance that has this NAME, or NIL if none."
  `(find-transform-named ',name))

(defparameter *attributes* nil
  "A convenience special/dynamic variable that references the attributes
of the element being transformed. The alternative is to specify the
lambda list for a deftag or define-transform to make the attributes
directly availible.")

(defun att (key)
  "A convenience function to retrieve a single attribute value from the
transforming element's attributes. See *attributes*"
  (if (and (consp *attributes*)
           (keywordp (car *attributes*)))
      (getf *attributes* key)
      (let ((att (find key *attributes* :key 'key :test #'equal)))
        (when att
          (value att)))))

(defun atts ()
  "A convenience function to get the attributes of the currently
transforming element. See *attributes*"
  *attributes*)

(defparameter *new-children* nil
  "A convenience special/dynamic variable that references the set of new/coppied
child elements created for the transformation in post-walk. The alternative is to
specify the lambda list for a deftag or define-transform to make the
attributes directly availible.")

(defun new-children ()
  "A convenience function to get the new/coppied chldren created for the transformatio in post-walk. See *new-children*"
  *new-children*)

(defparameter *element* nil
  "A conveniece special/dynamic variable that references the element
being transformed. The alternative is to specify the lambda list for a
deftag or define-transform to make the attributes directly availible.")

(defun element ()
  "A convenience function to get the element currently being transformed. See *elemen*"
  *element*)

(defparameter *path* nil
  "A conveniece special/dynamic variable that references the path
being transformed. The alternative is to specify the lambda list for a
deftag or define-transform to make the path directly availible.")

(defun path ()
  "A convenience function to get the path to the tag currently being transformed. See *path*"
  *path*)

(defgeneric perform-transform (transform dom)
  (:documentation "Transforms the DOM.

It performs a post-walk of the DOM tree, building for each element, a
list of new children by appending the lists of elements returned by
the transform function.

Then if the transform selector selects the element, it calls the
transform function on the element and the new children to get a
replacement element list.

Otherwise, if the new children is equal to the old children list, then
the element is returned unchanged, or else, a new element is built
with the element tag and attributes, and the new children.

"))

(define-condition no-such-transform (error)
  ((name :initarg :name :reader no-such-transform-name))
  (:report (lambda (condition stream)
             (format stream "There is no transform named ~S"
                     (no-such-transform-name condition)))))

(defmethod perform-transform ((name symbol) dom)
  (let ((transform (find-transform-named name)))
    (unless transform
      (error 'no-such-transform :name name))
    (perform-transform transform dom)))

(defmethod perform-transform ((transform transform) (dom element))
  (let* ((selector (transform-selector transform))
         (transfun (transform-function transform))
         (new-dom  (post-walk
                    dom
                    (lambda (element new-children path)
                      (let ((*element* element)
                            (*attributes* (attributes element))
                            (*new-children* new-children)
                            (*path* path))
                        (cond
                          ((select selector element :path path)
                           (funcall transfun element new-children path))
                          ((equalp (children element) new-children)
                           element)
                          (t
                           (make-instance 'element
                                          :tag (tag element)
                                          :attributes (attributes element)
                                          :children new-children)))))
                    '())))
    (unless (typep new-dom 'element)
      (error "The new root element obtained by transforming the DOM ~S thru ~S should be of type ~S, not a ~S: ~S"
             dom (transform-name transform) 'element (type-of new-dom) new-dom))
    new-dom))

;; (define-transform name transform-selector transform-lambda-list . body)
;;
;; transform-selector := list-of-tags | lambda-expression | predicate-name
;; list-of-tags := list-of-keywords
;; selector-lambda-expression ::= (lambda (element &key path) . body)
;; predicate-name := symbol
;;
;; transform-lambda-list = (element new-children [&key path]) | (tag attributes children new-children [&key path])

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun lambda-expression-p (form)
    (and (consp form)
         (eq (car form) 'lambda)
         (consp (cdr form))
         (listp (cadr form))))

  (defun element-lambda-list-p (ll)
    (case (position '&key ll)
      ((2)       (= 4 (length ll)))
      ((nil)     (= 2 (length ll)))
      (otherwise nil)))

  (defun tac-lambda-list-p (ll)
    (case (position '&key ll)
      ((4)       (= 6 (length ll)))
      ((nil)     (= 4 (length ll)))
      (otherwise nil))))

;;(tag (&rest transform-lambda-list) &optional selector)

;;; Tracing transforms is useful to debug:

(defvar *traced-transforms* (make-hash-table))

(defgeneric transform-tracing (transform))
(defgeneric (setf transform-tracing) (flag transform))

(defmethod transform-tracing ((transform transform))
  (gethash transform *traced-transforms*))
(defmethod (setf transform-tracing) (flag (transform transform))
  (if flag
      (setf (gethash transform *traced-transforms*) t)
      (progn (remhash transform *traced-transforms*) nil)))

(defmethod transform-tracing ((name symbol))
  (let ((transform (find-transform-named name)))
    (unless transform
      (error 'no-such-transform :name name))
    (transform-tracing transform)))
(defmethod (setf transform-tracing) (flag (name symbol))
  (let ((transform (find-transform-named name)))
    (unless transform
      (error 'no-such-transform :name name))
    (setf (transform-tracing transform) flag)))

(defun trace-transforms* (names)
  (dolist (name names)
    (with-simple-restart (continue "Continue with remaining names")
      (setf (transform-tracing name) t))))
(defun untrace-transforms* (names)
  (dolist (name names)
    (with-simple-restart (continue "Continue with remaining names")
      (setf (transform-tracing name) nil))))

(defmacro trace-transform (&rest names)
  `(trace-transforms* ',names))
(defmacro untrace-transform (&rest names)
  `(untrace-transforms* ',names))

;; Note: transforms are not calling one another normally, so we'll print a flat trace.
(defun transform-enter (name element new-children path)
  (format *trace-output* "~&Entering transform:    ~S~
                          ~%    for element:       ~S~
                          ~%    with new children: ~{~<~%                       ~:;~S~>~^ ~}~
                          ~%    from path:         ~{~<~%                       ~:;~S~>~^ ~}~
                          ~2%"
          name element new-children path))

(defun transform-non-local-exit  (name element new-children path)
  (format *trace-output* "~&NON-LOCAL EXIT of the transform: ~S~
                          ~%    for element:       ~S~
                          ~%    with new children: ~{~<~%                       ~:;~S~>~^ ~}~
                          ~%    from path:         ~{~<~%                       ~:;~S~>~^ ~}~
                          ~2%"
          name element new-children path))

(defun transform-exit (name element new-children path result)
  (format *trace-output* "~&Exiting transform:   ~S~
                          ~%    for element:       ~S~
                          ~%    with new children: ~{~<~%                       ~:;~S~>~^ ~}~
                          ~%    from path:         ~{~<~%                       ~:;~S~>~^ ~}~
                          ~%    with result:       ~S~
                          ~2%"
          name element new-children path result))

(define-condition invalid-transform-lambda-list (program-error)
  ((lambda-list :initarg :lambda-list :reader invalid-transform-lambda-list-lambda-list)
   (transform-name :initform nil :initarg :transform-name :reader invalid-transform-lambda-list-transform-name))
  (:report (lambda (condition stream)
             (format stream "Invalid transform-lambda-list ~S~@[ for transform named ~S~]"
                     (invalid-transform-lambda-list-lambda-list condition)
                     (invalid-transform-lambda-list-transform-name condition)))))

(defmacro define-transform ((name transform-selector (&rest transform-lambda-list)) &body body)
  (check-type name symbol)
  (check-type transform-selector (or list symbol))
  (let ((velement      (gensym "element"))
        (vnew-children (gensym "new-children"))
        (vpath         (gensym "path"))
        (vresult       (gensym "result")))
    (multiple-value-bind (dom-body declarations docstring)
        (alexandria:parse-body body :documentation t)
      `(eval-when (:compile-toplevel :load-toplevel :execute)
         (setf (gethash ',name *transforms*)
               (make-instance 'transform
                              :name ',name
                              :selector ,(selector* transform-selector)
                              :function (lambda (,velement ,vnew-children ,vpath)
                                          ,@(when docstring (list docstring))
                                          (declare (ignorable ,velement ,vnew-children ,vpath))
                                          (when (transform-tracing ',name)
                                            (transform-enter ',name ,velement ,vnew-children ,vpath))
                                          (let ((,vresult ',vresult)) ; "unbound"
                                            (unwind-protect
                                                 ;; unwind-protect returns the results of the first form so it's like prog1
                                                 (setf ,vresult ,(cond
                                                                   ((= 0 (length transform-lambda-list))
                                                                    `(with-dom ,@dom-body))
                                                                   ((element-lambda-list-p transform-lambda-list)
                                                                    (if (= 4 (length transform-lambda-list))
                                                                        `(destructuring-bind (,@transform-lambda-list)
                                                                             (list ,velement ,vnew-children :path ,vpath)
                                                                           ,@declarations
                                                                           (with-dom ,@dom-body))
                                                                        `(destructuring-bind (,@transform-lambda-list)
                                                                             (list ,velement ,vnew-children)
                                                                           ,@declarations
                                                                           (with-dom ,@dom-body))))
                                                                   ((tac-lambda-list-p transform-lambda-list)
                                                                    (if (= 6 (length transform-lambda-list))
                                                                        `(destructuring-bind (,@transform-lambda-list)
                                                                             (list (tag ,velement)
                                                                                   (attributes ,velement)
                                                                                   (children ,velement)
                                                                                   ,vnew-children :path ,vpath)
                                                                           ,@declarations
                                                                           (with-dom ,@dom-body))
                                                                        `(destructuring-bind (,@transform-lambda-list)
                                                                             (list (tag ,velement)
                                                                                   (attributes ,velement)
                                                                                   (children ,velement)
                                                                                   ,vnew-children)
                                                                           ,@declarations
                                                                           (with-dom ,@dom-body))))
                                                                   (t (error 'invalid-transform-lambda-list
                                                                             :lambda-list transform-lambda-list
                                                                             :transform-name name))))

                                              (when (transform-tracing ',name)
                                                (if (eq ,vresult ',vresult)
                                                    (transform-non-local-exit ',name ,velement ,vnew-children ,vpath)
                                                    (transform-exit ',name ,velement ,vnew-children ,vpath ,vresult))))))))
         '(transform ,name)))))

(defun apply-transform-chain (chain dom)
  "Apply all the transforms in the CHAIN in turn on the DOM.
RETURN: the transformed DOM.
CHAIN: a list of transform instances or transform names."
  (dolist (transform chain dom)
    (setf dom (perform-transform transform dom))))
