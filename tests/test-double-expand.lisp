(in-package :cl-naive-dom.tests)


(defun merge-classes (&rest classes)
  (remove-duplicates (apply #'append classes) :test #'equalp))

(defun att-theme (key theme)
  "THEME is a list of key values or a lambda (aka a theme value selector)"
  (declare (ignore key theme))
  (or (att :color)
      #|(tag-theme-color :tw-alert (att :class))|#))

(deftag (:alert-box (element new-children))
  (:div :class (merge-classes (att :class)
                              (att-theme :color '(:tw-alert :color))
                              (att :corners)
                              '("px-6"
                                "py-4"
                                "border-0"
                                "rounded"
                                "relative"
                                "mb-4"))
        new-children))

(deftag (:alert-icon)
  (:span :class (or (att :class)
                    '("text-xl" "inline-block" "mr-5" "align-middle"))
         (:i :class (att :icon))))

(deftag (:alert-body)
  (:span :class (or (att-theme :class '(:tw-alert :body-class))
                    '("inline-block" "align-middle" "mr-8"))
         (new-children)))

(deftag (:alert-button)
  (:button :class (or
                   (att-theme :class '(:tw-alert :button-class))
                   '("absolute"
                     "bg-transparent"
                     "text-normal"
                     "font-semibold"
                     "leading-none"
                     "right-0"
                     "top-0"
                     "mt-5"
                     "mr-4"
                     "outline-none"
                     "focus:outline-none"))
           :onclick (or
                     (att-theme :onclick '(:tw-alert :button-click))
                     "this.parentNode.style.display=\"none\";")
           (:i :class (or (att-theme :icon '(:tw-alert :button-icon))
                          "fa fa-times"))))

(deftag (:tw-alert)
  (:alert-box :class (att :class)
              :color (att :color)
              :corners (att :corners)

              ;;icon-class overrides the body class completely.
              (:alert-icon :icon (att :icon)
                           :class (att :icon-class))
              ;;Body-Class overrides the body class completely.
              (:alert-body :class (att :body-class)
                           (new-children))
              (:alert-button :class (att :button-class)
                             :icon (att :button-icon))))


(testsuite :double-expand

  (testcase :expand-1
            :info "expand should expand expanded nodes"
            :expected '(:div :class ("px-6" "py-4" "border-0" "rounded" "relative" "mb-4")
                        (:span :class ("text-xl" "inline-block" "mr-5" "align-middle") (:i))
                        (:span :class ("inline-block" "align-middle" "mr-8") "Oh Fuck!")
                        (:button :class
                                 ("absolute" "bg-transparent" "text-normal" "font-semibold" "leading-none" "right-0" "top-0" "mt-5" "mr-4"
                                             "outline-none" "focus:outline-none")
                                 :onclick "this.parentNode.style.display=\"none\";" (:i :class "fa fa-times")))
            :actual (with-current-package
                        (with-sexp
                          (expand
                           (with-dom
                             (:tw-alert "Oh Fuck!"))))))

  (testcase :expand-2
            :info "expand should be idempotent"
            :expected '(:div :class ("px-6" "py-4" "border-0" "rounded" "relative" "mb-4")
                        (:span :class ("text-xl" "inline-block" "mr-5" "align-middle") (:i))
                        (:span :class ("inline-block" "align-middle" "mr-8") "Oh Fuck!")
                        (:button :class
                                 ("absolute" "bg-transparent" "text-normal" "font-semibold" "leading-none" "right-0" "top-0" "mt-5" "mr-4"
                                             "outline-none" "focus:outline-none")
                                 :onclick "this.parentNode.style.display=\"none\";" (:i :class "fa fa-times")))
            :actual (with-current-package
                        (with-sexp
                          (expand
                           (expand
                            (with-dom
                              (:tw-alert "Oh Fuck!")))))
                      )))

